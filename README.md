# HuerTechno

v1.1: backend

Este repositorio contiene la api-rest/backend de huertechno. Además se incluye la versión 
para Raspberry pi y x64. La versión en raspberry está basada en imagenes de raspbian.


En este repositorio se encuentran tres aplicaciones. 

 - app-test: emula un dispositivo nodeMCU que envía mediciones cada 5 segundos mediante
el protocolo mqtt
 - app-save: aplicación cliente-mqtt que almacena las mediciones de de los cultivos
y los guarda en la base de datos (postgresql)
 - app-ht: aplicación de huertechno que registra cultivos, microcontroladores y 
componentes; expone las mediciones de la base de datos

## Levantar aplicación - docker-compose.yml

El archivo yml tiene los servicios:

 - broker mqtt: eclipse-mosquitto
 - base de datos: postgresql-9.6
 - python:3.5: aplicaciones.

Las variables se pueden configurar del archivo setup.sh.

### POSTGRES

 - POSTGRES_USER=pi
 - POSTGRES_PASSWORD=pi123
 - POSTGRES_DB=huertechno

### app-ht
 - POSTGRES_USER=pi
 - POSTGRES_PASSWORD=pi123
 - POSTGRES_DB=huertechno
 - POSTGRES_HOST=db

### app-save
 - POSTGRES_USER=pi
 - POSTGRES_PASSWORD=pi123
 - POSTGRES_DB=huertechno
 - POSTGRES_HOST=db
 - BROKER_MQTT=mqtt


## RUN

``` docker-compose build && docker-compose up ```


## Recursos-Entrypoint

### Cultivo
Información del cultivo, se entiende por cultivo a las plantas que compartan
los mismos cuidados:

    - nombre: Nombre del cultivo
    - tipo cultivo: Hidroponia o Tierra
    - estado: on/off -> Mididiendo o no.

#### Metodos

    - curl http://<ip host>:8080/api/cultivo GET
    - curl http://<ip host>:8080/api/cultivo -X POST -d 'name=<nombre>&type_cultivo=<tipo>&state=<estado>' 
    - curl http://<ip host>:8080/api/cultivo -X PUT -d  'id=<id cultivo>&name=<nombre>&type_cultivo=<tipo>&state=<estado>'
    - curl http://<ip host>:8080/api/cultivo -X DELETE -d 'id=<id cultivo>'


### Device
Información del dispositivo que publica en el broker las mediciones o se suscribe
a las acciones:

    - nombre: Nombre del dispositivo
    - model: nodemcu, esp8266, ...
    - marca: nodemcu, arduino uno ...
    - state: on/off -> Mididiendo o no.
    - cultivo: lista de cultivos separados por coma (id_cultivo1;
    id_cultivo2;id_cultivo3);foreign key de cultivo

#### Metodos

    - curl http://<ip host>:8080/api/device GET
    - curl http://<ip host>:8080/api/device -X POST -d 'name=<nombre>&model=&marca=&=state=&cultivo<lista ;>' 
    - curl http://<ip host>:8080/api/device -X PUT -d  'id=<id device>name=<nombre>&model=&marca=&=state=&cultivo<lista ;>'
    - curl http://<ip host>:8080/api/device -X DELETE -d 'id=<id device>'
    
    
    
### Component
Información de un componente, como un sensor o como un actuador (bomba de riego):

    - name: Nombre del componente DHT22Temperatura
    - model: dht22, ...
    - variable: Temperatura, Humedad
    - function: <sensor action>
    - state: on/off -> Mididiendo o no.
    - num_pin: Numero del pin en el dispositivo

#### Metodos

    - curl http://<ip host>:8080/api/component GET
    - curl http://<ip host>:8080/api/component -X POST -d ''' 
    - curl http://<ip host>:8080/api/component -X PUT -d  ''
    - curl http://<ip host>:8080/api/component -X DELETE -d ''
    
    


### HuertaAll
Tabla de mediciones, metodo get trae todas las mediciones registradas y mediante 
POST puede traer de un periodo de tiempo **date_start** y **date_end**:

    - value: Nombre del componente DHT22Temperatura
    - date_created: fecha
    - id_cultivo: dht22, ...

#### Metodos

    - curl http://<ip host>:8080/api/huerta/all GET
    - curl http://<ip host>:8080/api/huerta/all -X POST -d 'date_start=2019-01-13%2017:50&date_end=2019-01-13%2017:55' 

    
    
### Huerta
Tabla de mediciones. A diferencia del recurso anterior, este recurso puede traer un 
numero de mediciones (n_med).

    - value: Nombre del componente DHT22Temperatura
    - date_created: fecha
    - id_cultivo: dht22, ...

#### Metodos

    - curl http://<ip host>:8080/api/huerta/<n_med> GET


    
    