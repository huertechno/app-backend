from models import Measure, Component
from base import Session
from datetime import datetime

session = Session()

measures = session.query(Measure).all()
components = session.query(Component) \
            .join(Measure, Component.measure) \
            .all()


print('\n### All measure')

for measure in measures:
    print({measure.value}, {measure.component_id})


result = dict()

print('\n### Measure')
for component in components:
    print(component.name, component.variable)
    resultados = component.measure

print(resultados)


components_id = session.query(Component) \
                .filter(Component.id == 1) \
                .first()


print(components_id.id, components_id.vari)
