from models import Measure, Component, Base
from config import sqlalchemy_database_uri, broker_mqtt, port_mqtt
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine(sqlalchemy_database_uri)
Session = sessionmaker(bind = engine)

Base.metadata.create_all(engine)

def init():

    session = Session()

    component1 = Component("dhtTemperatura", "dht22", "Temperatura", "sensor", 2, "on")
    component2 = Component("dhtHumedad", "dht22", "Humedad", "sensor", 2, "on")

    session.add(component1)
    session.add(component2)

    session.commit()
    session.close()


if __name__ == '__main__':
    init()
