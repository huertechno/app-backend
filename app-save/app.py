from config import sqlalchemy_database_uri, broker_mqtt, port_mqtt, sub_topic
from sqlalchemy import create_engine
from sqlalchemy.sql import exists
import paho.mqtt.client as mqtt
from datetime import datetime

engine = create_engine(sqlalchemy_database_uri)


def measure_save(connection, msg):
    trans = connection.begin()
    #ex = connection.execute(
    #    "SELECT * FROM component WHERE EXISTS (SELECT * FROM COMPONENT WHERE id= %s)", \
    #    msg.topic.split('/')[1]
    #)
    #print("query:{}".format(ex))
    #if ex[0]:
    try:
        m1 = connection.execute(
            "INSERT INTO measure (date_created, value, sensor_id) VALUES (%s, %s, %s)", \
            datetime.now(), msg.payload.decode("utf-8"), msg.topic.split('/')[1])
        trans.commit()
    except:
        #return str("no se puso guardar la medicion, quizas no hay registrado un component")
        trans.rollback()



def main():
    global connection
    connection = engine.connect()
    client = mqtt.Client()
    client.connect(broker_mqtt, int(port_mqtt))
    client.on_message = on_message
    client.on_connect = on_connect
    client.loop_forever()



def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+ str(rc))
    client.subscribe(sub_topic)



def on_message(client, userdata, msg):
    print(msg.topic + ":" + str(msg.payload))
    measure_save(connection, msg)



