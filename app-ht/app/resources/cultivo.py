from flask import request
from flask import jsonify
from flask_restful import  Resource
from app.common.database import session
from app.models import Cultivo as CultivoDb


class Cultivo(Resource):

    def get(self):
        cultivos = [c._asdict() for c in session.query(CultivoDb).all()]
        return jsonify(cultivos)

    def post(self):
        args_cultivo = request.get_json()
        cultivo = CultivoDb(args_cultivo['name'] \
            ,args_cultivo['type_cultivo'] \
            ,args_cultivo['state'])
        session.add(cultivo)
        session.commit()
        return dict(args_cultivo)

    def delete(self):
        args_cultivo = request.get_json()
        try:
            delete_cultivo = session.query(CultivoDb) \
                            .filter_by(id=args_cultivo['id']) \
                            .first()
            session.delete(delete_cultivo)
            session.commit()
        except:
            return("No se ha podido eliminar: " + str(args_cultivo['id']))
        return str("Se ha eliminado el cultivo: " + str(args_cultivo['id']))

    def put(self):
        args_cultivo = request.get_json()
        values = {'name':args_cultivo['name'], \
         'type_cultivo':args_cultivo['type_cultivo'], \
         'state':args_cultivo['state']}
        session.query(CultivoDb).filter_by(id=args_cultivo['id']) \
        .update(values)
        session.commit()
        return str("se actualizo correctamente")


