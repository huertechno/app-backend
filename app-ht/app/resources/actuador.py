from flask import request
from flask import jsonify
from flask_restful import Resource
from app.common.database import session
from datetime import datetime
from app.models import Device as DeviceDb
from app.models import Actuador as ActuadorDb



class Actuador(Resource):

    def get(self):
        try:
            actuadores = [act._asdict() for act in session.query(ActuadorDb).all()]
        except:
            return str("No se encuentran actuadores registrados")
        return jsonify(actuadores)

    def post(self):
        args_actuador = request.get_json()
        if args_actuador:
            actuador = ActuadorDb(
            args_actuador['name'], \
            args_actuador['model'], \
            args_actuador['num_pin'], \
            args_actuador['state'])

        if  args_actuador['device']:
            device_args = args_actuador['device']
            devices = [session.query(DeviceDb) \
                .filter_by(id=d_id) \
                .first() for d_id in device_args]
            actuador.device = devices
        session.add(actuador)
        session.commit()
        #return str("no se pudotar actuador")
        return dict(args_actuador)

    def delete(self):
        args_actuador = request.get_json()
        del_actuador = session.query(ActuadorDb) \
                            .filter_by(id=args_actuador['id']) \
                            .first()
        session.delete(del_actuador)
        session.commit()
            #return("No se ha podido eliminar: " + args_actuador['id'])
        return str("Se ha eliminado el actuador: " + args_actuador['id']), 204

    def put(self):
        args_actuador = request.get_json()
        values = {
            'name': args_actuador['name'], \
            'model': args_actuador['model'], \
            'num_pin': args_actuador['num_pin'], \
            'state': args_actuador['state'], \
            }
        actuador_update = session.query(ActuadorDb) \
        .filter_by(id=args_actuador['id'])

        if  args_actuador['device']:
            device_args = args_actuador['device']
            print("device: {}".format(device_args))
            devices = [session.query(DeviceDb) \
                .filter_by(id=d_id) \
                .first() for d_id in device_args]

        actuador_update.update(values)
        actuador_update[0].device = devices
        session.commit()
        return str("se actualizo correctamente")

