from flask import request
from flask import jsonify
from flask_restful import Resource
from app.common.database import session
from flask import jsonify
from app.models import Device as DeviceDb
from app.models import Actuador as ActuadorDb
from app.models import Sensor as SensorDb



class Sensor(Resource):
    """
    Sensor: recurso que se refiere a los sensores tales como sensores.
    """
    def get(self):
        try:
            sensors = [c._asdict() for c in session.query(SensorDb).all()]
        except:
            return str("No se encuentran sensores registrados")
        return jsonify(sensors)

    def post(self):
        args_sensor = request.get_json()
        sensor = SensorDb(args_sensor['name'], \
            args_sensor['model'], \
            args_sensor['variable'], \
            args_sensor['state'], \
            args_sensor['num_pin'])
        if  args_sensor['device']:
            device_args = args_sensor['device']
            devices = [session.query(DeviceDb) \
                .filter_by(id=d_id) \
                .first() for d_id in device_args]
            sensor.device = devices
        session.add(sensor)
        session.commit()
        return dict(args_sensor)

    def delete(self):
        args_sensor = request.get_json()
        args_sensor = parser_sensor.parse_args()
        del_sensor = session.query(SensorDb) \
                            .filter_by(id=args_sensor['id']) \
                            .first()
        session.delete(del_sensor)
        session.commit()
        return str("Se ha eliminado el sensor: " + str(args_sensor['id'])), 204


    def put(self):
        args_sensor = request.get_json()
        device_args = args_sensor['device']
        device_add = [session.query(DeviceDb) \
        .filter_by(id=d_id) \
        .first() for d_id in device_args]
        values = {
            'model': args_sensor['model'], \
            'variable': args_sensor['variable'], \
            'state':args_sensor['state'], \
            'num_pin':args_sensor['num_pin'] \
            }
        sensor_update = session.query(SensorDb) \
        .filter_by(id=args_sensor['id'])
        sensor_update.update(values)
        sensor_update[0].device = device_add
        session.commit()
        return str("se actualizo correctamente")




