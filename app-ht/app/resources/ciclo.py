from flask import request
from flask import jsonify
from flask_restful import  Resource
from app.common.database import session
from app.models import  CicloVegetativo as CicloDb
from datetime import datetime
import json


class Ciclo(Resource):

    def get(self):
        cveg = [cv._asdict() for cv in session.query(CicloDb).all()]
        return jsonify(cveg)

    def post(self):
        args_ciclo = request.get_json()
        descripcion = args_ciclo['descripcion']
        ciclo = CicloDb(descripcion)
        session.add(ciclo)
        session.commit()
        return jsonify(descripcion)

    def delete(self):
        args_ciclo = request.get_json()
        id_ciclo = args_ciclo['id']
        delete_ciclo = session.query(CicloDb) \
                       .filter_by(id=id_ciclo) \
                       .first()
        session.delete(delete_ciclo)
        session.commit()
        return str("se elimino")

    def put(self):
        args_ciclo = request.get_json()
        descripcion = args_ciclo['descripcion']
        id_ciclo = args_ciclo['id']
        session.query(CicloDb) \
        .filter_by(id=id_ciclo) \
        .update({"descripcion":descripcion})
        session.commit()
        return str("se actualizo")



class CicloEvent(Resource):

    def get(self):
        pass


class CicloVariable(Resource):
    pass
