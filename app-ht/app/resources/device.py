from flask import request
from flask import jsonify
from flask_restful import Resource
from app.common.database import session
from app.models import Device as DeviceDb
from app.models import Cultivo as CultivoDb


class Device(Resource):

    def get(self):
        devices = [c._asdict() for c in session.query(DeviceDb).all()]
        return jsonify(devices)

    def post(self):
        args_device = request.get_json()
        cultivos_args = args_device['cultivo']
        cultivo =[session.query(CultivoDb) \
        .filter_by(id=c_id) \
        .first() for c_id in cultivos_args]
        device = DeviceDb(args_device['name'], \
            args_device['model'], \
            args_device['marca'], \
            args_device['state'])
        device.cultivo = cultivo
        session.add(device)
        session.commit()
        return  str(args_device['name'])

    def delete(self):
        args_device = request.get_json()
        del_device = session.query(DeviceDb) \
                            .filter_by(id=args_device['id']) \
                            .first()
        session.delete(del_device)
        session.commit()
        return str("Se ha eliminado el device: " + str(args_device['id']))

    def put(self):
        args_device = request.get_json()
        cultivos_args = args_device['cultivo']
        cultivo =[session.query(CultivoDb) \
        .filter_by(id=c_id) \
        .first() for c_id in cultivos_args]
        values = {
            'name':args_device['name'], \
            'model':args_device['model'], \
            'marca':args_device['marca'], \
            'state':args_device['state'], \
            }
        device_update = session.query(DeviceDb) \
        .filter_by(id=args_device['id'])
        device_update.update(values)
        device_update[0].cultivo = cultivo
        session.commit()
        return str("se actualizo correctamente")

