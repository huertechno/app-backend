from flask_restful import reqparse, Resource
from app.common.database import session
from app.models import Measure
from datetime import datetime
from flask import jsonify
import time

parser_sensor = reqparse.RequestParser()
parser_sensor.add_argument('date_start')
parser_sensor.add_argument('date_end')
parser_sensor.add_argument('n_med')

class HuertaAll(Resource):

    def get(self):
        measure = [m._asdict() for m in session.query(Measure).all()]
        return jsonify(measure)


    def post(self):
        args_sensor = parser_sensor.parse_args()
        date_start = datetime.strptime(args_sensor['date_start'], "%Y-%m-%d %H:%M")
        date_end = datetime.strptime(args_sensor['date_end'], "%Y-%m-%d %H:%M")
        measure = [m._asdict() for m in session \
                .query(Measure) \
                .filter(Measure.date_created.between(date_start, date_end)) \
                .all()]
        return jsonify(measure)



class Huerta(Resource):

    def get(self, n_med):
        measure = [m._asdict() for m in session.query(Measure).all()][-int(n_med):]
        return jsonify(measure)


