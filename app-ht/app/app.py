from flask_restful import reqparse, Resource, Api, abort
from flask import Flask, jsonify, request
from app.common.database import init_db 
from app.resources.cultivo import Cultivo
from app.resources.device  import Device
from app.resources.sensor import Sensor
from app.resources.actuador import Actuador
from app.resources.huerta import Huerta, HuertaAll
from app.resources.ciclo import Ciclo



app = Flask(__name__)
api = Api(app)
init_db()



api.add_resource(Cultivo, '/api/v1/cultivo')
api.add_resource(Device, '/api/v1/device')
api.add_resource(Huerta, '/api/v1/huerta/<n_med>')
api.add_resource(HuertaAll, '/api/v1/huerta/all')
api.add_resource(Sensor, '/api/v1/sensor')
api.add_resource(Actuador, '/api/v1/actuador')
api.add_resource(Ciclo, '/api/v1/ciclo')
#api.add_resource(SensorActuador, '/api/v1/variables')
