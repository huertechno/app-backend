from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from app.config import sqlalchemy_database_uri


engine = create_engine(sqlalchemy_database_uri)

Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
session = scoped_session(Session)

Base = declarative_base()

def init_db():
    import app.models
    Base.metadata.create_all(engine)
