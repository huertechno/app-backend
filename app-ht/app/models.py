from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.sql.expression import func
from sqlalchemy.orm import relationship
from app.common.database import Base
from sqlalchemy import  (Table, Column,
    Integer, DateTime, ForeignKey, String)




class BaseDb(Base):

    __abstract__ = True

    id = Column(Integer, primary_key=True)
    date_created  = Column(DateTime,  default=func.current_timestamp())

    def _asdict(self):
        result = dict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result



cultivo_device = Table('cultivo_device', Base.metadata,
    Column('cultivo_id', Integer, ForeignKey('cultivo.id')),
    Column('device_id', Integer, ForeignKey('device.id'))
)


device_sensor = Table('device_sensor', Base.metadata,
    Column('device_id', Integer, ForeignKey('device.id')),
    Column('sensor_id', Integer, ForeignKey('sensor.id'))
)


device_actuador = Table('device_actuador', Base.metadata,
    Column('device_id', Integer, ForeignKey('device.id')),
    Column('actuador_id', Integer, ForeignKey('actuador.id'))
)



ciclo_cultivo = Table('ciclo_cultivo', Base.metadata,
    Column('ciclo_vegetativo_id', Integer, ForeignKey('ciclo_vegetativo.id')),
    Column('cultivo_id', Integer, ForeignKey('cultivo.id'))
)




class Cultivo(BaseDb):

    __tablename__ = 'cultivo'

    name = Column(String)
    type_cultivo = Column(String)
    state = Column(String)

    ciclo_vegetativo = relationship(
        "CicloVegetativo",
        secondary = ciclo_cultivo,
        back_populates = "cultivo")


    def __init__(self, name, type_cultivo, state):
        self.name = name
        self.type_cultivo = type_cultivo
        self.state = state


class Device(BaseDb):

    __tablename__ = 'device'

    name = Column(String)
    model = Column(String)
    marca = Column(String)
    state = Column(String)
    cultivo = relationship(
        "Cultivo",
        secondary = cultivo_device,
        backref = "device")
    sensor = relationship(
        "Sensor",
        secondary = device_sensor,
        back_populates = "device")
    actuador = relationship(
        "Actuador",
        secondary = device_actuador,
        back_populates = "device")


    def __init__(self, name, model, marca, state):
        self.name = name
        self.model = model
        self.marca = marca
        self.state = state


class Sensor(BaseDb):

    __tablename__ = 'sensor'

    name = Column(String)
    model = Column(String)
    variable = Column(String)
    state = Column(String)
    num_pin = Column(String)

    measure = relationship("Measure", backref="sensor")
    device = relationship(
        "Device",
        secondary = device_sensor,
        back_populates = "sensor")


    def __init__(self, name, model, variable, state, num_pin):
        self.name = name
        self.model = model
        self.variable = variable
        self.state = state
        self.num_pin = num_pin




class Measure(BaseDb):

    __tablename__= 'measure'

    value = Column(String, nullable=False)
    sensor_id = Column(Integer, ForeignKey('sensor.id'))

    def __init__(self, value, sensor_id):
        self.value = value
        self.sensor_id = sensor_id




class CicloVegetativo(BaseDb):

    __tablename__= 'ciclo_vegetativo'
    descripcion = Column(JSONB)
    cultivo = relationship(
        "Cultivo",
        secondary = ciclo_cultivo,
        back_populates = "ciclo_vegetativo")

    def __init__(self, descripcion):
        self.descripcion = descripcion




class Actuador(BaseDb):

    __tablename__= 'actuador'

    name = Column(String)
    model = Column(String)
    num_pin = Column(String)
    state = Column(String)
    accion = relationship("Accion", backref="actuador")
    device = relationship(
        "Device",
        secondary = device_actuador,
        back_populates = "actuador")


    def __init__(self, name, model, num_pin, state):
        self.name = name
        self.model = model
        self.num_pin = num_pin
        self.state = state





class Accion(BaseDb):

    __tablename__= 'accion'

    value = Column(String, nullable=False)
    actuador_id = Column(Integer, ForeignKey('actuador.id'))

    def __init__(self, value, actuador_id):
        self.value = value
        self.actuador_id = actuaor_id





class Calendar(BaseDb):

    __tablename__= 'calendar'
    action = Column(JSONB)

    def __init__(self, action):
        self.action = action
